package com.example.thymeliefjavacapii.configuration;


import com.example.thymeliefjavacapii.entity.ItemFactura;
import com.example.thymeliefjavacapii.entity.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class ApiConfiguration {


    @Bean("ItemsFactura")
    public List<ItemFactura> itemFacturas(){
        Product producto = new Product("nevera", 100);
        Product producto1 = new Product("lavadora", 200);

       ItemFactura itemFactura = new ItemFactura(producto,1);
       ItemFactura itemFactura1 = new ItemFactura(producto1, 2);

        return Arrays.asList(itemFactura,itemFactura1);
    }

}
