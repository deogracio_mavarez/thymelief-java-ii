package com.example.thymeliefjavacapii;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources(
        @PropertySource("classpath:factura.properties")
)
public class PropertiesConfiguration {
}
