package com.example.thymeliefjavacapii.controller;


import com.example.thymeliefjavacapii.entity.Factura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/factura")
public class FacturaController {


    @Autowired
    private Factura factura;

    @GetMapping("/ver")
    public String getFactura(Model model){

        model.addAttribute("titulo", "ejemplo factura con inyeccion de dependencia");
        model.addAttribute("factura", factura);
        return "factura/ver";
    }




}
