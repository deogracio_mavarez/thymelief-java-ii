package com.example.thymeliefjavacapii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeliefJavaCapiiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThymeliefJavaCapiiApplication.class, args);
    }

}
